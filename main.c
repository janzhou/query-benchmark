#include<stdio.h>
#include<stdlib.h>
#include<time.h>

#include "thread.h"
#include "sem.h"
#include "main.h"

#define random(x) (rand()%x)

main(){
    int i,j;
    
unsigned long long k = 0;
    init();
    srand((int)time(0));
    printf("start\n");
    for(i = 0; i < 10; i++){
        for(j = 0; j < 1024*1024*1024; j++){
            //query(random(MAX_LBA));
            k += query(random(MAX_PAG));
        }
    }
    printf("finish %llu\n", k);
}
