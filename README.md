当映射表全在内存时，我对块映射和页映射的性能差异做了一下分析和仿真。

可以估算到：华为SSD如果做页映射，映射表大约有1.6GB；如果做块映射，映射表
大约有6.5MB。大约是256:1的关系。

CPU的内部缓存会影响查表性能。i7的三级缓存有8～12MB；Xeon（服务器处理器）
的三级缓存有15～30MB。CPU的缓存完全有能力 把块映射的映射表缓存下来。理想
情况下，采用块映射时cache（CPU的三级缓存）命中率要比采用页映射时高256倍。

我用代码实测了一下。假设逻辑地址有1024*1024*1024*1024＝1T。若采用页映
射，则有1024*1024*128个映射关系， 每个映射关系至少占8字节，于是映射表有
1GB大小内存。若采用块映射，则有1024*512个映射关系，占4MB大小内存。

测试，做10*1024*1024*1024次随机地址的查表。采用页映射时，耗时8分45秒；采
用块映射时，耗时2分10秒。

这次测试没有考虑多核，我猜，如果充分利用多核，性能差异会更大（更好的利用
L1和L2和更大的并行计算能力都对块映射更有利）。

#page

real 8m45.765s
user 8m45.149s
sys 0m0.149s

#block

real    2m10.739s
user    2m10.613s
sys     0m0.005s
