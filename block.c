#include "main.h"

unsigned long long table[MAX_BLK];

void init(void){
    int i;
    for(i = 0; i < MAX_BLK; i++){
        table[i]=i;
    }
    printf("block test ");
}

unsigned long long query(int page){
    return table[page/256]+(page%256);
}
