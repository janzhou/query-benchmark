#ifndef _H_MAIN_
#define _H_MAIN_

#define MAX_LBA 1024*1024*1024*1024   //1T
#define MAX_PAG 1024*1024*128         //pages
#define MAX_BLK 1024*512              //blocks
#endif
