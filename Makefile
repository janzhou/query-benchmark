CFLAGS	= -Wall -W -O2 -g
LIBS		= -pthread
all= page block

all: $(all)

page: page.o sem.o thread.o main.o
	cc $(CFLAGS) -o $@ $^ $(LIBS)

block: block.o sem.o thread.o main.o
	cc $(CFLAGS) -o $@ $^ $(LIBS)

%.o: %.c
	$(CC) $(CFLAGS) -c -o $*.o $<

clean: 
	rm -f *.o
	rm -f $(all)
